﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atFrameWork2.TestEntities
{
    public class WishboxEvent
    {

        public string Title { get; set; }
        public string Date { get; set; }
        public string LinkToPicture { get; set; }
        public string EventPassword { get; set; }
    }
}
