﻿using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;
using OpenQA.Selenium;
using static System.Net.Mime.MediaTypeNames;

namespace atFrameWork2.PageObjects
{
    public class EventFrame
    {

        /// <summary>
        /// Фрейм добавления нового мероприятия (Название и дата)
        /// </summary>
        public EventFrame AddNewEvent(WishboxEvent testEvent)
        {

            var sliderFrame = new WebItem("//iframe[@class='side-panel-iframe']", "Фрейм слайдера");
            sliderFrame.SwitchToFrame();

            var EventName = new WebItem("//input[@class='ui-ctl-element' and @name='TITLE']", "Добавление названия мероприятия");
            EventName.SendKeys(testEvent.Title);

            var EventData = new WebItem("//input[@class='ui-ctl-element' and @name='EVENT_DATE']", "Добавление даты мероприятия");
            EventData.SendKeys(testEvent.Date);

            return this;
        }

        /// <summary>
        /// Добавление картинки мероприятия
        /// </summary>
        public EventFrame SetEventPicture(string linkToPicture)
        {
            var EventPicture = new WebItem("//input[@id='file_input_mfiEVENT_IMAGE' and @name='bxu_files[]']", "Добавление картинки мероприятия");
            EventPicture.SendKeys(linkToPicture);
            EventPicture.WaitElementDisplayed();
            return this;
        }

        /// <summary>
        /// Подтверждение создания мероприятия
        /// </summary>
        public PortalEventPage ConfirmEvent()
        {
            var btnEventComplete = new WebItem("//div[@class='add-button']", "Кнопка завершения создания мероприятия'");
            btnEventComplete.Click();
            return new PortalEventPage();
        }

        /// <summary>
        /// Выбор приватности мероприятия и передача пароля
        /// </summary>
        public EventFrame SelectPrivateEventAndSetPasswd(string eventPassword)
        {
            var btnPrivateEvent = new WebItem("//input[@id='private-event']", "Выбор приватности для мероприятия'");
            btnPrivateEvent.Click();

            var passwordCreate = new WebItem("//input[@name='PASSWORD']", "Ввод пароля для мероприятия'");
            passwordCreate.SendKeys(eventPassword);
            return this;
        }

        /// <summary>
        /// Изменение названия и даты мероприятия
        /// </summary>
        public EventFrame InputNewEventData(WishboxEvent updTestEvent)
        {
            var sliderFrame = new WebItem("//iframe[@class='side-panel-iframe']", "Фрейм слайдера");
            sliderFrame.SwitchToFrame();

            var EventName = new WebItem("//input[@class='ui-ctl-element' and @name='TITLE']", "Изменение названия подарка");
            EventName.Click();
            EventName.SendKeys(Keys.Control + "a");
            EventName.SendKeys(Keys.Delete);
            EventName.SendKeys(updTestEvent.Title);

            var EventData = new WebItem("//input[@class='ui-ctl-element' and @name='EVENT_DATE']", "Добавление новой даты");
            EventData.Click();
            EventData.SendKeys(updTestEvent.Date);

            return this;
        }
    }
}