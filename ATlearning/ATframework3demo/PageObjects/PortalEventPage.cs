﻿using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;
using ATframework3demo.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace atFrameWork2.PageObjects
{
    public class PortalEventPage
    {
        /// <summary>
        /// Клик на кнопку Добавить мероприятие
        /// </summary>
        public EventFrame ClickButtonAddEvent()
        {
            var btnAddEvent = new WebItem("//a[@id='form-btn']", "Кнопка Создать мероприятие");
            btnAddEvent.Click();
            return new EventFrame();
        }

        /// <summary>
        /// Открытие созданного мероприятия (поиск по переданному названию)
        /// </summary>
        public EventGiftPage OpenCreatedEvent(string title)
        {
            var btnOpenEvent = new WebItem($"//a[@class='event-title' and text()='{title}']", "Кнопка открытия мероприятия");
            btnOpenEvent.Click();
            return new EventGiftPage();
        }

        /// <summary>
        /// Клик на кнопку Пригласить друзей(поиск по переданному названию)
        /// </summary>
        public InviteUserFrame ClickButtonInviteUser(string title)
        {
            var btnInviteUserToEvent = new WebItem($"//a[@class='event-title' and text()='{title}']/ancestor::div[contains(@class, 'event-item')]/descendant::a[@data-role='invite']", "Кнопка приглашения друга");
            btnInviteUserToEvent.Click();
            return new InviteUserFrame();
        }

        /// <summary>
        /// Переключение на вкладку "Мероприятия друзей"
        /// </summary>
        public FriendsEventsPage ClickFriendEvent()
        {
            var btnFriendEvent = new WebItem("//a[text()='Мероприятия друзей']", "Кнопка Мероприятия друзей");
            btnFriendEvent.Click();
            return new FriendsEventsPage();
        }

        /// <summary>
        /// Проверка на регистрацию пользователя. Если переданный логин найден возвращает true
        /// </summary>
        internal bool IsUserLoginExist(string login, IWebDriver driver = default)
        {
            var LoginElement = new WebItem($"//div[@class='wish-login' and text()='{login}']", "Поиск имени зарегистированного пользователя");
            LoginElement.WaitElementDisplayed();
            return LoginElement.GetAttribute("innerHTML", driver) == login;
        }


        /// <summary>
        /// Получение надписи о закрытости мероприятия.
        /// </summary>
        public string GetCreatedPrivateEventTypeOfClose()
        {
            var privateTypeOfClose = new WebItem("//div[contains(text(),'Закрытое')]", "Надпись о закрытости мероприятия'");
            return privateTypeOfClose.GetAttribute("innerText");
        }

        /// <summary>
        /// Клик на кнопку "Редактировать"
        /// </summary>
        public EventFrame ClickButtonEditEvent(string title)
        {
            var eventItemEditButton = new WebItem($"//a[@class='event-title' and text()='{title}']/ancestor::div[@class='event-item']//a[text()='Изменить']", "Кнопка редактирования мероприятия");
            eventItemEditButton.Click();
            return new EventFrame();
        }

        /// <summary>
        /// Получение имени последнего созданного мероприятия.
        /// </summary>
        public string GetLastCreatedEventName()
        {
            var eventName = new WebItem("//a[@class='event-title']", "Заголовок мероприятия'");
            return eventName.GetAttribute("innerText");
        }

        /// <summary>
        /// Удаление мероприятия.
        /// </summary>
        public PortalEventPage DeleteEvent(string title)
        {
            var eventItemEditButton = new WebItem($"//a[@class='event-title' and text()='{title}']/ancestor::div[@class='event-item']//a[text()='Удалить']", "Кнопка Удаления мероприятия");
            eventItemEditButton.Click();
            return this;
        }

        /// <summary>
        /// Получение ссылки мероприятия, ее копирование, выход из аккаунта, переход по ссылке
        /// </summary>
        public PasswordPagePrivateEvent CopyLink(object nameOfPrivateEvent)
        {
            var buttonItem = new WebItem($"//a[contains(@onclick, '{nameOfPrivateEvent}') and contains(@onclick, 'copyLink')]", "Кнопка копирования ссылки");
            var onClickAttributeValue = buttonItem.GetAttribute("onclick");
            var eventId = onClickAttributeValue.Substring(onClickAttributeValue.IndexOf("/event/") + 7);
            eventId = eventId.Substring(0, eventId.IndexOf("/"));
            Uri uri = new Uri($"http://wishbox/event/{eventId}/");
            var btnLogout = new WebItem("//a[@class='wish-logout']", "Кнопка выхода с аккаунта");
            btnLogout.Click();
            DriverActions.OpenUri(uri);
            return new PasswordPagePrivateEvent();
        }
    }
}