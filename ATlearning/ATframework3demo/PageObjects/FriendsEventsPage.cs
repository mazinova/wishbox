﻿using atFrameWork2.SeleniumFramework;

namespace atFrameWork2.PageObjects
{
    public class FriendsEventsPage
    {
        /// <summary>
        /// Клик на Принять приглашение. Поиск по переданному названию.
        /// </summary>
        public FriendsEventsPage ClickAcceptUserInvitation(string title)
        {            
            var btnAcceptUserInvitation = new WebItem($"//a[@class='event-title' and text()='{title}']/ancestor::div[contains(@class, 'event-main-info')]/descendant::a[contains(@id, 'form-btn-delete') and contains(@href, 'action=accept')]", "Кнопка принятия приглашения на мероприяте друга");
            btnAcceptUserInvitation.Click();
            return new FriendsEventsPage();
        }

        /// <summary>
        /// Открытие события на которое пользователь был приглашен. Поиск по переданному названию.
        /// </summary>
        public EventGiftPage ClickOpenInvitedEvent(string title)
        {
            var btnOpenInvitedEvent = new WebItem($"//a[@class='event-title' and text()='{title}']", "Кнопка открытия мероприятия");
            btnOpenInvitedEvent.Click();
            return new EventGiftPage();
        }

        /// <summary>
        /// Клик на Отклонить приглашение. Поиск по переданному названию.
        /// </summary>
        public FriendsEventsPage ClickDeclineUserInvitation(string title)
        {
            var btnAcceptUserInvitation = new WebItem($"//a[@class='event-title' and text()='{title}']/ancestor::div[contains(@class, 'event-main-info')]/descendant::a[contains(@id, 'form-btn-delete') and contains(@href, 'action=denied')]", "Кнопка отклонения приглашения на мероприятие друга");
            btnAcceptUserInvitation.Click();
            return this;
        }

        /// <summary>
        /// Получение имени последнего принятого мероприятия.
        /// </summary>
        internal string GetInvitedEventName()
        {
            var invitedEventName = new WebItem("//a[@class='event-title']", "Заголовок принятого мероприятия'");
            return invitedEventName.GetAttribute("innerText");
        }

        /// <summary>
        /// Получение имени последнего принятого мероприятия.
        /// </summary>
        internal string GetDeclinedEventName()
        {
            var declinedEventName = new WebItem("//a[@class='event-title']", "Заголовок последнего мероприятия'");
            return declinedEventName.GetAttribute("innerText");
        }
    }
}