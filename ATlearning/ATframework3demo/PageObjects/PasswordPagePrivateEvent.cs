﻿using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;
using OpenQA.Selenium;

namespace ATframework3demo.PageObjects
{
    public class PasswordPagePrivateEvent
    {
        public PasswordPagePrivateEvent FillPassword(string text, IWebDriver driver = default)
        {
            var inputPassword = new WebItem("//input[@placeholder='Пароль']", "Поле ввода пароля'");
            inputPassword.SendKeys(text);
            return new PasswordPagePrivateEvent();
        }

        public EventGiftPage ConfirmPassword()
        {
            var btnConfirmPassword = new WebItem("//input[@class='btn']", "Кнопка подтверждения пароля'");
            btnConfirmPassword.Click();
            return new EventGiftPage();
        }
    }
}
