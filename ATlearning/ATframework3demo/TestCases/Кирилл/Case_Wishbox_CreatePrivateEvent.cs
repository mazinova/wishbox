﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;


namespace ATframework3demo.TestCases.Кирилл
{
    public class CreatePrivateEvent : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var CaseCollection = new List<TestCase>();
            CaseCollection.Add(new TestCase("Создание приватного мероприятия", homePage => CreateCloseEvent(homePage)));
            return CaseCollection;
        }

        void CreateCloseEvent(PortalHomePage homePage)
        {
            Random rnd = new Random();
            string typeOfClose = "Закрытое | C паролем";
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = DateTime.Now.ToString("HH-mm-ss dd-MM-yyyy");
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");
            testEvent.EventPassword = "test" + rnd.Next(100, 1000);


            var CreatePrivateEvent = homePage
                .EventPage
                //Нажать на "создать мероприятие"
                .ClickButtonAddEvent()
                //Ввести название мероприятия
                .AddNewEvent(testEvent)
                //Создать приватность для мероприятия
                .SelectPrivateEventAndSetPasswd(testEvent.EventPassword)
                //Нажать кнопку "создать ивент"
                .ConfirmEvent();



            //Проверить, что мероприятие создалось, и что оно является закрытым
            string createdPrivateEventName = CreatePrivateEvent.GetLastCreatedEventName();
            string createEventCloseType = CreatePrivateEvent.GetCreatedPrivateEventTypeOfClose();
            if (createdPrivateEventName == testEvent.Title)
            {
                Log.Info("Название мероприятия совпадает с ожидаемым");
            }
            else
            {
                Log.Error("Название мероприятия не совпадает с ожидаемым");
            }
            if (createEventCloseType == typeOfClose)
            {
                Log.Info("Мероприятие является закрытым");
            }
            else
            {
                Log.Error("Мероприятие не является закрытым");
            }
        }
    }
}
