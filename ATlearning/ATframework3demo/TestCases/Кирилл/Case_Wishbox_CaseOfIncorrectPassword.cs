﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.TestEntities;


namespace ATframework3demo.TestCases.Кирилл
{
    public class AutoTestOfFillingIncorrectPassword : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var CaseCollection = new List<TestCase>();
            CaseCollection.Add(new TestCase("Ввод неправильного пароля на мероприятие", homePage => FillingOfIncorrectPassword(homePage)));
            return CaseCollection;
        }

        void FillingOfIncorrectPassword(PortalHomePage homePage)
        {
            Random rnd = new Random();
            string notification = "Неверный пароль!";
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = DateTime.Now.ToString("HH-mm-ss dd-MM-yyyy");
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");
            testEvent.EventPassword = "test" + rnd.Next(100, 1000);

            var WrongEventPassword = "TEST";

            var CreatePrivateEvent = homePage
                .EventPage
                //Нажать на "создать мероприятие"
                .ClickButtonAddEvent()
                //Ввести название мероприятия
                .AddNewEvent(testEvent)
                //Создать приватность для мероприятия
                .SelectPrivateEventAndSetPasswd(testEvent.EventPassword)
                //Нажать кнопку "создать ивент"
                .ConfirmEvent();

            var OpenPrivateEvent = CreatePrivateEvent
                //Скопировать ссылку выйти с аккаунта и перейти по ссылке
                .CopyLink(testEvent.Title)
                //передать пароль
                .FillPassword(WrongEventPassword)
                //Подтвердить пароль
                .ConfirmPassword();

            //Проверить уведомление о неправильном пароле
            string TitleNotification = OpenPrivateEvent.GetNotificationAboutWrongPassword();
            if (TitleNotification == notification)
            {
                Log.Info("Уведомление о неправильном пароле появилось");
            }
            else
            {
                Log.Error("Уведомление о неправильном пароле не появилось");
            }
        }
    }
}
