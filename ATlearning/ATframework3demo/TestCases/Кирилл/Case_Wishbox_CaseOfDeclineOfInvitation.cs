﻿using atFrameWork2.BaseFramework;
using atFrameWork2.PageObjects;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.TestEntities;
using System.Diagnostics.Tracing;


namespace ATframework3demo.TestCases.Кирилл
{
    public class TestOfDeclineOfInvitation : CaseCollectionBuilder
    {
        private PortalInfo testPortal;
        protected override List<TestCase> GetCases()
        {
            var CaseCollection = new List<TestCase>();
            CaseCollection.Add(new TestCase("Отклонение приглашения на мероприятие", homePage => DeclineInvite(homePage)));
            return CaseCollection;
        }

        void DeclineInvite(PortalHomePage homePage)
        {
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = DateTime.Now.ToString("HH-mm-ss dd-MM-yyyy");
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");

            User user2 = new User();
            user2.Login = "admin2";
            user2.Password = "123456";

            var CreateEvent = homePage
                .EventPage
                //Нажать на "создать мероприятие"
                .ClickButtonAddEvent()
                //Ввести название мероприятия
                .AddNewEvent(testEvent)
                //Нажать кнопку "создать ивент"
                .ConfirmEvent();

            var InviteUser = CreateEvent
            //Нажать кнопку "пригласить друга"
                .ClickButtonInviteUser(testEvent.Title)
            //Ввести имя друга
                .InviteUser(user2.Login)
            //Выйти из меню приглашения
                .ClickCloseFrame();

            var ChangeUsers = homePage
                .AboveMenu
                //выход с аккаунта
                .ClickLogout(testPortal)
                //Перезаход с акаунта admin2
                .ReLogin(user2);

            var DeclineEvent = homePage
                .EventPage
                //клик на "мероприятия друзей"
                .ClickFriendEvent()
                //клик на принять
                .ClickDeclineUserInvitation(testEvent.Title);


            //Проверить, что отклонение приглашения прошло успешно
            string declinedEventName = DeclineEvent.GetDeclinedEventName();
            if (declinedEventName == testEvent.Title)
            {
                Log.Error("Отклонение приглашения не прошло успешно");
            }
            else
            {
                Log.Info("Отклонение приглашения прошло успешно");
            }
        }

    }

}
