﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.TestEntities;

namespace ATframework3demo.TestCases.Кирилл
{
    public class AutoTestOfFillingCorrectPassword : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var CaseCollection = new List<TestCase>();
            CaseCollection.Add(new TestCase("Ввод правильного пароля на мероприятие", homePage => FillingOfCorrectPassword(homePage)));
            return CaseCollection;
        }

        void FillingOfCorrectPassword(PortalHomePage homePage)
        {
            Random rnd = new Random();
            string typeOfClose = "Закрытое | C паролем";
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = DateTime.Now.ToString("HH-mm-ss dd-MM-yyyy");
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");
            testEvent.EventPassword = "test" + rnd.Next(100, 1000);



            var CreatePrivateEvent = homePage
                .EventPage
                //Нажать на "создать мероприятие"
                .ClickButtonAddEvent()
                //Ввести название мероприятия
                .AddNewEvent(testEvent)
                //Создать приватность для мероприятия
                .SelectPrivateEventAndSetPasswd(testEvent.EventPassword)
                //Нажать кнопку "создать ивент"
                .ConfirmEvent();

            var OpenPrivateEvent = CreatePrivateEvent
                //Скопировать ссылку выйти с аккаунта и перейти по ссылке
                .CopyLink(testEvent.Title)
                //передать пароль
                .FillPassword(testEvent.EventPassword)
                //Подтвердить пароль
                .ConfirmPassword();

            //Проверить, что мероприятие создалось, и что оно является закрытым
            string giftsPageTitle = OpenPrivateEvent.GetTitleOfGiftsPage();
            string expectedTitle = $"{testEvent.Title}: ПОДАРКИ (0)";

            if (giftsPageTitle.Contains(expectedTitle))
            {
                Log.Info("Переход на созданное мероприятие произошел успешно");
            }
            else
            {
                Log.Error($"Значение заголовка страницы подарков не соответствует ожидаемому, ожидаемое значение: '{expectedTitle}', фактическое: '{giftsPageTitle}'");
            }
        }
    }
}