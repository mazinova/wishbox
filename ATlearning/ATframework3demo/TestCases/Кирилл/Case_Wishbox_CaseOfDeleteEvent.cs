﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.TestEntities;

namespace ATframework3demo.TestCases.Кирилл
{
    public class TestOfDeleteEvent : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var CaseCollection = new List<TestCase>();
            CaseCollection.Add(new TestCase("Удаление мероприятия", homePage => DeleteEvent(homePage)));
            return CaseCollection;
        }

        void DeleteEvent(PortalHomePage homePage)
        {
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = DateTime.Now.ToString("HH-mm-ss dd-MM-yyyy");
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");
            var DeleteOfEvent = homePage
                .EventPage
                //Нажать на "создать мероприятие"
                .ClickButtonAddEvent()
                //Ввести название мероприятия
                .AddNewEvent(testEvent)
                //Нажать кнопку "создать ивент"
                .ConfirmEvent()
                //Нажать кнопку удаления
                .DeleteEvent(testEvent.Title);
                //проверить удаление мероприятия

            string LastNameEvent = DeleteOfEvent.GetLastCreatedEventName();
            if (LastNameEvent == testEvent.Title)
            {
                Log.Error("Мероприятие не удалено");
            }
            else
            {
                Log.Info("Мероприятие удалено");
            }
        }
    }
}
