﻿using atFrameWork2.BaseFramework;
using atFrameWork2.PageObjects;
using atFrameWork2.BaseFramework.LogTools;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using atFrameWork2.TestEntities;

namespace ATframework3demo.TestCases.Кирилл
{
    public class TestOfCreateEvent : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var CaseCollection = new List<TestCase>();
            CaseCollection.Add(new TestCase("Создание открытого мероприятия с картинкой", homePage => CreateEventWithPic(homePage)));
            return CaseCollection;
        }

        void CreateEventWithPic(PortalHomePage homePage)
        {      
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = DateTime.Now.ToString("HH-mm-ss dd-MM-yyyy");
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");
            testEvent.LinkToPicture = "D:\\Загрузки\\pic.jpg";

            var CreateEventWithPic = homePage
                 .EventPage
                 // Нажать на "создать мероприятие"
                 .ClickButtonAddEvent()
                 // переключение фрейма, передача данных и нажатие на Enter, возвращение фрейма
                 .AddNewEvent(testEvent)
                 //загрузить фотографию
                 .SetEventPicture(testEvent.LinkToPicture)
                 //нажать кнопку создать
                 .ConfirmEvent();




            //Проверить, что мероприятие создалось
            string createdEventName = CreateEventWithPic.GetLastCreatedEventName();
            if (createdEventName == testEvent.Title)
            {
                Log.Info("Название мероприятия совпадает с ожидаемым");
            }
            else
            {
                Log.Error("Название мероприятия не совпадает с ожидаемым");
            }
        }
    }
}

