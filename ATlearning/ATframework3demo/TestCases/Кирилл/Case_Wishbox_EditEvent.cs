﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.TestEntities;


namespace ATframework3demo.TestCases.Кирилл
{
    public class CaseOfEditEvent : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var CaseCollection = new List<TestCase>();
            CaseCollection.Add(new TestCase("Редактирование мероприятия", homePage => EditEvent(homePage)));
            return CaseCollection;
        }

        void EditEvent(PortalHomePage homePage)
        {            
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = DateTime.Now.ToString("HH-mm-ss dd-MM-yyyy");
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");

            WishboxEvent updTestEvent = new WishboxEvent();
            updTestEvent.Title = DateTime.Now.AddDays(1).ToString("HH-mm-ss dd-MM-yyyy");
            updTestEvent.Date = DateTime.Now.AddDays(6).ToString("dd-MM-yyyy");

            var CreateEvent = homePage
                .EventPage
                // Нажать на "создать мероприятие"
                .ClickButtonAddEvent()
                //Ввести название мероприятия и дату проведения
                .AddNewEvent(testEvent)
                //Нажать кнопку "создать ивент"
                .ConfirmEvent();


            var EditPresent = CreateEvent
                //Нажать "изменить"
                .ClickButtonEditEvent(testEvent.Title)
                //Ввести новое название
                .InputNewEventData(updTestEvent)
                //Нажать "изменить ивент"
                .ConfirmEvent();

            // Проверка, что мероприятие было создано и изменено
            string createdEventEditName = EditPresent.GetLastCreatedEventName();
            if (createdEventEditName == testEvent.Title)
            {
                Log.Error("Мероприятие не отредактировано");
            }
            else
            {
                Log.Info("Мероприятие отредактировано");
            }
        }
    }
}
