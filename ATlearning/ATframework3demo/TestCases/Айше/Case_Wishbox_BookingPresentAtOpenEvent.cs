﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;


namespace ATframework3demo.TestCases.Айше
{
    public class Case_Wishbox_BookingPresent : CaseCollectionBuilder
    {
        private PortalInfo testPortal;

        protected override List<TestCase> GetCases()
        {
            var caseCollection = new List<TestCase>();
            caseCollection.Add(new TestCase("Бронирование подарка", homePage => BookingPresentAtOpenEvent(homePage)));
            return caseCollection;
        }

        void BookingPresentAtOpenEvent(PortalHomePage homePage)
        {
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = "Название мероприятия " + DateTime.Now;
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");

            var PresentData = new WishboxPresent("Название подарка " + DateTime.Now, "Ссылка", "какое-то описание");
            User user2 = new User();
            user2.Login = "admin2";
            user2.Password = "123456";

            var CreateEvent = homePage
                .EventPage
                // клик на добавить мероприятие
                .ClickButtonAddEvent()
                // переключение фрейма, передача данных и нажатие на Enter, возвращение фрейма
                .AddNewEvent(testEvent)

                .ConfirmEvent();

            var AddPresent = CreateEvent
                // Открытие на мероприятия
                .OpenCreatedEvent(testEvent.Title)
                // Клик на добавитьь подарок
                .ClickButtonAddPresent()
                // переключение фрейма, передача данных и нажатие на Enter, возвращение фрейма
                .AddPresent(PresentData);

            //отправление приглашение на мероприятие:
            var InviteToEvent = homePage
                .AboveMenu
                //возврат к мероприятию(клик на кнопку мероприятия сверху)
                .OpenEventsPage()
                //клик на пригласить пользователя
                .ClickButtonInviteUser(testEvent.Title)
                //ввод логина admin2 +нажатие enter
                .InviteUser(user2.Login)
                //закрыть фрейм(можно заменить обновлением страницы)
                .ClickCloseFrame();

            var ChangeUsers = homePage
                .AboveMenu
                //выход с аккаунта
                .ClickLogout(testPortal)
                //Перезаход с акаунта admin2
                .ReLogin(user2);

            var AcceptEvent = homePage
                .EventPage
                //клик на "мероприятия друзей"
                .ClickFriendEvent()
                //клик на принять
                .ClickAcceptUserInvitation(testEvent.Title);

            var ReservePresent = AcceptEvent
                //клик на подробнее
                .ClickOpenInvitedEvent(testEvent.Title)
                //клик на бронировать
                .ClickReservePresent(PresentData.Title);



            // Проверка заключается в переходе к вкладке мои подарки и просмотру там данного подарка
            var ReservedPresent = homePage
                .AboveMenu
                .ReservedPresent();

            bool isPresentReserved = ReservedPresent.IsPresentReserved(PresentData.Title);
            if (isPresentReserved == false)
            {
                Log.Error($"Подарок с названием {PresentData.Title} не был зарезервирован");
            }
            else
            {
                Log.Info($"Подарок с названием {PresentData.Title} был успешно зарезервирован");
            }
        }
    }
}
