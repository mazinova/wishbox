﻿using atFrameWork2.BaseFramework;
using atFrameWork2.BaseFramework.LogTools;
using atFrameWork2.PageObjects;
using atFrameWork2.SeleniumFramework;
using atFrameWork2.TestEntities;
using Microsoft.AspNetCore.Mvc.Diagnostics;

namespace ATframework3demo.TestCases
{
    public class Case_Wishbox_AddPresent : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var caseCollection = new List<TestCase>();
            caseCollection.Add(new TestCase("Добавление подарка", homePage => CreatePresent(homePage)));
            return caseCollection;
        }

        void CreatePresent(PortalHomePage homePage)
        {
            WishboxEvent testEvent = new WishboxEvent();
            testEvent.Title = "Название мероприятия " + DateTime.Now;
            testEvent.Date = DateTime.Now.AddDays(7).ToString("dd-MM-yyyy");

            var PresentData = new WishboxPresent("Название подарка " + DateTime.Now, "Ссылка", "какое-то описание");

            var CreateEvent = homePage
                .EventPage
                // клик на добавить мероприятие
                .ClickButtonAddEvent()
                // переключение фрейма, передача данных и нажатие на Enter, возвращение фрейма
                .AddNewEvent(testEvent)

                .ConfirmEvent();

            var AddPresent = CreateEvent
                // Открытие на мероприятия
                .OpenCreatedEvent(testEvent.Title)
                // Клик на добавитьь подарок
                .ClickButtonAddPresent()
                // переключение фрейма, передача данных и нажатие на Enter, возвращение фрейма
                .AddPresent(PresentData);


            // Проверка что подарок появился
            // ищем элемент с переданным именем

            bool isPresentNameExist = AddPresent.IsPresentNameExist(PresentData.Title);
            if (isPresentNameExist == false)
            {
                Log.Error($"Подарка с названием {PresentData.Title} нет");

            }
            else
            {
                Log.Info($"Подарок с названием {PresentData.Title} успешно создан");
            }
        }
    }
}
